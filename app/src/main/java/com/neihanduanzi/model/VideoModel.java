package com.neihanduanzi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 齐泽威 on 2016/10/31.
 */

public class VideoModel {
    @SerializedName("tip")
    private String tip;
    @SerializedName("data")
    private List<Fdata> mFdatas;
    public String getTip() {
        return tip;
    }
    public List<Fdata> getFdatas() {
        return mFdatas;
    }
    
    public class Fdata {
        @SerializedName("type")
        private int type;
        @SerializedName("group")
        private VideoDetial mVideoDetial;
        public VideoDetial getVideoDetial() {
            return mVideoDetial;
        }
        public int getType() {
            return type;
        }

        public class VideoDetial {
            @SerializedName("id")
            private long id;
            @SerializedName("group_id")
            private long group_id;
            @SerializedName("user")
            private UserInfo user;
            @SerializedName("title")
            private String title;
            @SerializedName("category_name")
            private String category_name;
            @SerializedName("mp4_url")
            private String mp4_url;
            @SerializedName("digg_count")
            private int digg_count; //点赞
            @SerializedName("bury_count")
            private int bury_count;//踩
            @SerializedName("repin_count")
            private int repin_count;//回复数
            @SerializedName("share_count")
            private int share_count;//分享数
            //视频封面
            @SerializedName("large_cover")
            private RecommendBean.Cover mCover;
            @SerializedName("video_height")
            private int video_height;

            public int getVideo_height() {
                return video_height;
            }
            public RecommendBean.Cover getCover() {
                return mCover;
            }
            public int getBury_count() {
                return bury_count;
            }
            public int getDigg_count() {
                return digg_count;
            }
            public int getRepin_count() {
                return repin_count;
            }
            public int getShare_count() {return share_count;}
            public String getMp4_url() {
                return mp4_url;
            }
            public String getCategory_name() {
                return category_name;
            }
            public String getTitle() {
                return title;
            }
            public UserInfo getUser() {
                return user;
            }
            public long getId() {
                return id;
            }
            public long getGroup_id() {
                return group_id;
            }
            
            public class Cover{
                @SerializedName("uri")
                private String uri;
                @SerializedName("url_list")
                private List<RecommendBean.Cover.UrlListBean> url_list;

                public String getUri() {
                    return uri;
                }

                public void setUri(String uri) {
                    this.uri = uri;
                }

                public List<RecommendBean.Cover.UrlListBean> getUrl_list() {
                    return url_list;
                }

                public void setUrl_list(List<RecommendBean.Cover.UrlListBean> url_list) {
                    this.url_list = url_list;
                }

                public  class UrlListBean {
                    @SerializedName("url")
                    private String url;

                    public String getUrl() {
                        return url;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }
                }
            }
            public class UserInfo{
                @SerializedName("name")
                private String name;
                @SerializedName("avatar_url")
                private String iconUrl;

                public String getIconUrl() {
                    return iconUrl;
                }
                public String getName() {
                    return name;
                }
            }

        }
    }


}


