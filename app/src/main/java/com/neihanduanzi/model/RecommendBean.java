package com.neihanduanzi.model;

/**
 * Created by 凸显 on 2016/10/30.
 */

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**type=2  视频   3动图(图片 动图(is_gif =1) 段子)
 * 视频详情的实体类
 */

public class RecommendBean {
    @SerializedName("id")
    private long id;
    @SerializedName("type")
    private int type;
    @SerializedName("user")
    private User user;
    @SerializedName("text")
    private String title;
    //类型名称  如:搞笑视频  二次元
    @SerializedName("category_name")
    private String category_Name;
    //分享数
    @SerializedName("share_count")
    private int shareCount;
    @SerializedName("360p_video")
    private V360pVideo mV360pVideo;
    //评论数
    @SerializedName("comment_count")
    private int commentCount;
    //赞
    @SerializedName("digg_count")
    private int diggCount;
    //踩
    @SerializedName("bury_count")
    private int buryCount;

    @SerializedName("is_gif")
    private int isGif;
    //图片
    @SerializedName("middle_image")
    private Image mImage;
    @SerializedName("large_image")
    private Image mImageGif;
    //播放次数
    @SerializedName("play_count")
    private int playCount;
    //视频封面
    @SerializedName("large_cover")
    private Cover mCover;

    @SerializedName("group_id")
    private long groupId;

    @SerializedName("content")
    private String content;

    private Comments mComments;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public Image getImageGif() {
        return mImageGif;
    }

    public void setImageGif(Image imageGif) {
        mImageGif = imageGif;
    }

    public Cover getCover() {
        return mCover;
    }

    public void setCover(Cover cover) {
        mCover = cover;
    }

    public Comments getComments() {
        return mComments;
    }

    public void setComments(Comments comments) {
        mComments = comments;
    }

    public int getPlayCount() {
        return playCount;
    }

    public void setPlayCount(int playCount) {
        this.playCount = playCount;
    }

    public int getIsGif() {
        return isGif;
    }

    public void setIsGif(int isGif) {
        this.isGif = isGif;
    }

    public Image getImage() {
        return mImage;
    }

    public void setImage(Image image) {
        mImage = image;
    }

    public int getBuryCount() {
        return buryCount;
    }

    public void setBuryCount(int buryCount) {
        this.buryCount = buryCount;
    }

    public String getCategory_Name() {
        return category_Name;
    }

    public void setCategory_Name(String category_Name) {
        this.category_Name = category_Name;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getDiggCount() {
        return diggCount;
    }

    public void setDiggCount(int diggCount) {
        this.diggCount = diggCount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public V360pVideo getV360pVideo() {
        return mV360pVideo;
    }

    public void setV360pVideo(V360pVideo v360pVideo) {
        mV360pVideo = v360pVideo;
    }

    public int getShareCount() {
        return shareCount;
    }

    public void setShareCount(int shareCount) {
        this.shareCount = shareCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "RecommendBean{" +
                "buryCount=" + buryCount +
                ", id=" + id +
                ", type=" + type +
                ", user=" + user +
                ", title='" + title + '\'' +
                ", category_Name='" + category_Name + '\'' +
                ", shareCount=" + shareCount +
                ", mV360pVideo=" + mV360pVideo +
                ", commentCount=" + commentCount +
                ", diggCount=" + diggCount +
                ", isGif=" + isGif +
                ", mImage=" + mImage +
                ", mComments=" + mComments +
                '}';
    }

    public class Cover{
        @SerializedName("uri")
        private String uri;
        @SerializedName("url_list")
        private List<UrlListBean> url_list;

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public List<UrlListBean> getUrl_list() {
            return url_list;
        }

        public void setUrl_list(List<UrlListBean> url_list) {
            this.url_list = url_list;
        }

        public  class UrlListBean {
            @SerializedName("url")
            private String url;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }
    }

    //图片类
    public class Image{
        @SerializedName("width")
        private int width;
        @SerializedName("r_height")
        private int r_height;
        @SerializedName("r_width")
        private int r_width;
        @SerializedName("uri")
        private String uri;
        @SerializedName("height")
        private int height;

        @SerializedName("url_list")
        private List<UrlListBean> url_list;

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getR_height() {
            return r_height;
        }

        public void setR_height(int r_height) {
            this.r_height = r_height;
        }

        public int getR_width() {
            return r_width;
        }

        public void setR_width(int r_width) {
            this.r_width = r_width;
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public List<UrlListBean> getUrl_list() {
            return url_list;
        }

        public void setUrl_list(List<UrlListBean> url_list) {
            this.url_list = url_list;
        }

        @Override
        public String toString() {
            return "Image{" +
                    "height=" + height +
                    ", width=" + width +
                    ", r_height=" + r_height +
                    ", r_width=" + r_width +
                    ", uri='" + uri + '\'' +
                    ", url_list=" + url_list +
                    '}';
        }

        public  class UrlListBean {
            @SerializedName("url")
            private String url;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            @Override
            public String toString() {
                return "UrlListBean{" +
                        "url='" + url + '\'' +
                        '}';
            }
        }
    }
    //视频类
    public class V360pVideo{
        @SerializedName("width")
        private int width;
        @SerializedName("uri")
        private String uri;
        @SerializedName("height")
        private int height;
        @SerializedName("url_list")
        private List<UrlListBean> url_list;

        @Override
        public String toString() {
            return "V360pVideo{" +
                    "height=" + height +
                    ", width=" + width +
                    ", uri='" + uri + '\'' +
                    ", url_list=" + url_list +
                    '}';
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public List<UrlListBean> getUrl_list() {
            return url_list;
        }

        public void setUrl_list(List<UrlListBean> url_list) {
            this.url_list = url_list;
        }

        public  class UrlListBean {
            @SerializedName("url")
            private String url;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            @Override
            public String toString() {
                return "UrlListBean{" +
                        "url='" + url + '\'' +
                        '}';
            }
        }
    }
}
