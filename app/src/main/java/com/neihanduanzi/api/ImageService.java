package com.neihanduanzi.api;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Administrator on 2016/11/1 0001.
 */

public interface ImageService {

    @GET("stream/mix/v1/?content_type=-103")
    Call<String> getImageList();
}
