package com.neihanduanzi.api;


import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by 凸显 on 2016/10/31.
 */

public interface RecommendService {
    @GET("?content_type=-101&message_cursor=-1&loc_time=1432654641&latitude=40.0522901291784&longitude=116.23490963616668&city=%E5%8C%97%E4%BA%AC&count=30&screen_width=800&iid=2767929313&device_id=2757969807&ac=wifi&channel=baidu2&aid=7&app_name=joke_essay&version_code=400&device_platform=android&device_type=KFTT&os_api=15&os_version=4.0.3&openudid=b90ca6a3a19a78d6")
    Call<String> getRecommendData();

}
