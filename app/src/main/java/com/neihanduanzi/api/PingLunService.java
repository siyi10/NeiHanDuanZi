package com.neihanduanzi.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by 齐泽威 on 2016/11/1.
 */

public interface PingLunService {

    @GET("2/data/get_essay_comments/")
    Call<String>getPingLun(@Query("group_id") long id);
}
