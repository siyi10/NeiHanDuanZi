package com.neihanduanzi.api;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by 齐泽威 on 2016/10/31.
 */

public interface VideoService {

    @GET("v1/?content_type=-104&message_cursor=-1")
    Call<String> FirstRequst();
}
