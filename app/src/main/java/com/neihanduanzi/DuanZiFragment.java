package com.neihanduanzi;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.neihanduanzi.model.DuanZi;
import com.neihanduanzi.model.GroupId;
import com.neihanduanzi.myview.CircleImageView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

public class DuanZiFragment extends Fragment {

    private CircleImageView user_image;
    private TextView userName;
    private TextView content;
    private TextView shareFrom;
    private CheckBox Digg;
    private CheckBox Bury;
    private CheckBox Discuss;
    private CheckBox Share;
    private DuanZi.Data duanZi;



    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,
                             Bundle savedInstanceState) {
        View ret = inflater.inflate(R.layout.item_duanzi, container, false);
        String duanZi = getActivity().getIntent().getStringExtra("Data");
        Gson gson = new Gson();
        final DuanZi.Data.Group data = gson.fromJson(duanZi, DuanZi.Data.Group.class);
        user_image = (CircleImageView) ret.findViewById(R.id.user_image);
        userName = (TextView) ret.findViewById(R.id.user_name);
        content = (TextView) ret.findViewById(R.id.content_neihan);
        shareFrom = (TextView) ret.findViewById(R.id.share_from);
        Digg = (CheckBox) ret.findViewById(R.id.duanzi_zan);
        Bury = (CheckBox) ret.findViewById(R.id.duanzi_cai);
        Discuss = (CheckBox) ret.findViewById(R.id.duanzi_discuss_count);
        Share = (CheckBox) ret.findViewById(R.id.shar_view);
        Share.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                EventBus.getDefault().post(data);
            }
        });

        if (user_image != null) {
            String cover = data.getUser().getAvatar_url();
            if (cover != null) {
                Context context = user_image.getContext();
                Picasso.with(context)
                        .load(cover)
                        .resize(320, 200)
                        .config(Bitmap.Config.ARGB_8888)
                        .into(user_image);
            }
        }
        userName.setText(data.getUser().getName());
        content.setText(data.getContent());
        shareFrom.setText(data.getCategory_name());
        Digg.setText(data.getDigg_count() + "");
        Bury.setText(data.getBury_count() + "");
        Discuss.setText(data.getComment_count() + "");
        Share.setText(data.getShare_count() + "");


        GroupId groupId = new GroupId();
        groupId.setGroup_id(data.getGroup_id());
        Log.d("aaa", "onCreate: " + data.getGroup_id());
        EventBus.getDefault().post(groupId);
        return ret;
    }


}
