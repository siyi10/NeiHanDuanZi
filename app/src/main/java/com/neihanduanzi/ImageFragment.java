package com.neihanduanzi;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.neihanduanzi.model.ImageModel;
import com.neihanduanzi.myview.CircleImageView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

public class ImageFragment extends Fragment {
    private  CircleImageView userImage;
    private  TextView userName;
    private  TextView content;
    private  TextView shareFrom;
    private  ImageView image;
    private  CheckBox digg;
    private  CheckBox bury;
    private  CheckBox discussCount;
    private  CheckBox share;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View ret = inflater.inflate(R.layout.item_image, container, false);

        String extra = getActivity().getIntent().getStringExtra("Data");
        Gson gson = new Gson();
        final ImageModel.Data.Group data = gson.fromJson(extra.toString(), ImageModel.Data.Group.class);

        userImage = (CircleImageView)ret.findViewById(R.id.user_image);
        userName = (TextView)ret.findViewById(R.id.user_name);
        content = (TextView)ret.findViewById(R.id.content_view);
        shareFrom = (TextView)ret.findViewById(R.id.image_from);
        image = (ImageView)ret.findViewById(R.id.image_view);
        digg = (CheckBox)ret.findViewById(R.id.image_digg);
        bury = (CheckBox)ret.findViewById(R.id.image_bury);
        discussCount = (CheckBox)ret.findViewById(R.id.image_discuss_count);
        share = (CheckBox)ret.findViewById(R.id.shar_view);
        share.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                EventBus.getDefault().post(data);
            }
        });

        if (userImage != null){
            String cover = data.getUser().getAvatar_url();
            if (cover != null){
                Context context = userImage.getContext();
                Picasso.with(context)
                        .load(cover)
                        .config(Bitmap.Config.ARGB_8888)
                        .into(userImage);
            }
        }
        userName.setText(data.getUser().getName());
        content.setText(data.getContent());
        shareFrom.setText(data.getCategory_name());

        if (image != null){
            String url = data.getMiddle_image().getUrl_list().get(0).getUrl();
            if (url != null){
                Context context = image.getContext();
                Picasso.with(context)
                        .load(url)
                        .into(this.image);
            }

            if (data.getIs_gif() == 1){
                String urlGif = data.getLarge_image().getUrl_list().get(0).getUrl();
                if (urlGif != null){
                    Context context = image.getContext();
                    Glide.with(context)
                            .load(urlGif)
                            .asGif()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(image);
                }
            }
        }
        digg.setText(data.getDigg_count() + "");
        bury.setText(data.getBury_count() + "");
        discussCount.setText(data.getComment_count() + "");
        share.setText(data.getShare_count() + "");


        return ret ;
    }
}
