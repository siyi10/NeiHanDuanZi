package com.neihanduanzi;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.neihanduanzi.adapter.CommonFragmentPagerAdapter;
import com.neihanduanzi.fragment.BaseFragment;
import com.neihanduanzi.fragment.load.CollectFragment;
import com.neihanduanzi.fragment.load.CommentFragment;
import com.neihanduanzi.fragment.load.ContributeFragment;
import com.neihanduanzi.myview.CircleImageView;

import java.util.ArrayList;
import java.util.List;

public class LoadActivity extends AppCompatActivity implements View.OnClickListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CommonFragmentPagerAdapter mAdapter;
    private ImageButton back;
    private ImageView mode;
    private CircleImageView login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);

        back = (ImageButton) findViewById(R.id.back_view);
        mode = (ImageView) findViewById(R.id.night_mode);
        login = (CircleImageView) findViewById(R.id.login_view);

        tabLayout = (TabLayout)findViewById(R.id.tablayout_view);
        viewPager = (ViewPager) findViewById(R.id.viewpager_view);

        if (viewPager != null){
            List<BaseFragment> fragments = new ArrayList<>();
            fragments.add(new CollectFragment());
            fragments.add(new CommentFragment());
            fragments.add(new ContributeFragment());

            mAdapter = new CommonFragmentPagerAdapter(getSupportFragmentManager(), fragments);
            viewPager.setAdapter(mAdapter);
            tabLayout.setupWithViewPager(viewPager);
        }

        back.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back_view:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.login_view:
                Intent intent1 = new Intent(this, EnterActivity.class);
                startActivity(intent1);
                break;
        }

    }


}


