package com.neihanduanzi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.neihanduanzi.R;
import com.neihanduanzi.model.ImageModel;
import com.neihanduanzi.myview.CircleImageView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Administrator on 2016/11/2 0002.
 */

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private List<ImageModel.Data> modelList;

    public ImageAdapter(Context mContext, List<ImageModel.Data> modelList) {
        this.mContext = mContext;
        this.modelList = modelList;
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = null;
        if(convertView != null){
            ret = convertView;
        }else {
            ret = LayoutInflater.from(mContext).inflate(R.layout.item_image, parent, false);
        }
        ViewHolder holder = (ViewHolder)ret.getTag();
        if (holder == null) {
            holder = new ViewHolder(ret);
            ret.setTag(holder);
        }
        holder.bindView(position, modelList.get(position));
        return ret;
    }


    private class ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private final CircleImageView userImage;
        private final TextView userName;
        private final TextView content;
        private final TextView shareFrom;
        private final ImageView image;
        private final CheckBox digg;
        private final CheckBox bury;
        private final CheckBox discussCount;
        private final CheckBox share;
        private ImageModel.Data mData;

        public ViewHolder(View itemView) {

            userImage = (CircleImageView)itemView.findViewById(R.id.user_image);
            userName = (TextView)itemView.findViewById(R.id.user_name);
            content = (TextView)itemView.findViewById(R.id.content_view);
            shareFrom = (TextView)itemView.findViewById(R.id.image_from);
            image = (ImageView)itemView.findViewById(R.id.image_view);
            digg = (CheckBox)itemView.findViewById(R.id.image_digg);
            bury = (CheckBox)itemView.findViewById(R.id.image_bury);
            discussCount = (CheckBox)itemView.findViewById(R.id.image_discuss_count);
            share = (CheckBox)itemView.findViewById(R.id.shar_view);
            share.setOnCheckedChangeListener(this);
        }

        public void bindView(int position, ImageModel.Data data) {
            mData = data;
            SharedPreferences sharedPreferences = mContext.getSharedPreferences("FontSetting", Context.MODE_PRIVATE);
            float font1 = sharedPreferences.getFloat("movieFont1", 18);
            float font2 = sharedPreferences.getFloat("movieFont2", 20);
            if (data.getType() == 1){
                if (userImage != null){
                    String cover = data.getGroup().getUser().getAvatar_url();
                    if (cover != null){
                        Context context = userImage.getContext();
                        Picasso.with(context)
                                .load(cover)
                                .config(Bitmap.Config.ARGB_8888)
                                .into(userImage);
                    }
                }

                content.setTextSize(font1);
                userName.setText(data.getGroup().getUser().getName());
                content.setText(data.getGroup().getContent());
                shareFrom.setText(data.getGroup().getCategory_name());

                if (image != null){
                    String url = data.getGroup().getMiddle_image().getUrl_list().get(0).getUrl();
//                    int height = data.getGroup().getMiddle_image().getHeight();
//                    int width = data.getGroup().getMiddle_image().getWidth();
//                    image.setMaxHeight(height);
//                    image.setMaxWidth(width);
                    if (url != null){
                        Context context = image.getContext();
                        Picasso.with(context)
                                .load(url)
                                .config(Bitmap.Config.ARGB_8888)
                                .into(image);
                    }

                    if (data.getGroup().getIs_gif() == 1){
                        String urlGif = data.getGroup().getLarge_image().getUrl_list().get(0).getUrl();
                        if (urlGif != null){
                            Context context = image.getContext();
                            Glide.with(context)
                                    .load(urlGif)
                                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                    .into(image);
                        }
                    }
                }

                digg.setText(data.getGroup().getDigg_count() + "");
                bury.setText(data.getGroup().getBury_count() + "");
                discussCount.setText(data.getGroup().getComment_count() + "");
                share.setText(data.getGroup().getShare_count() + "");
            }
        }


        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            EventBus.getDefault().post(mData);
        }
    }
}
